const {EventEmitter} = require('events');
const {getUuc} = require('./tiktokUtils');
const pkg = require('../../package.json');


const axios = require('axios').create({
    timeout: 5000,
    headers: {
        'User-Agent': `${pkg.name}/${pkg.version} ${process.platform}`,
    },
});

let config = {
    enabled: true,
    signProviderHost: 'https://new-sign-tt-aycoaohohf.us-west-1.fcapp.run',
    providerPath: "get_web_sign",
    key: "", // contact us[https://t.me/reverse4free2] for applying test key of our signature service D:).
    extraParams: {},
};

let signEvents = new EventEmitter();

function signWebcastRequest(url, headers, cookieJar) {
    return signRequest(config.providerPath, url, headers, cookieJar);
}

async function signRequest(providerPath, url, headers, cookieJar) {
    if (!config.enabled) {
        return url;
    }

    try {

        let data = {
            "key": config.key,
            "req_url": url
        }
        let signResponse = await axios.post(`${config.signProviderHost}/${providerPath}`, data);
        if (signResponse.status !== 200) {
            throw new Error(`Status Code: ${signResponse.status}`);
        }

        if (!signResponse.data?.data["signed_url"]) {
            throw new Error('missing signedUrl property');
        }

        if (cookieJar) {
            cookieJar.setCookie('msToken', signResponse.data.data['ms_token']);
        }

        let signedUrl = signResponse.data.data["signed_url"];

        if (headers) {
            headers['User-Agent'] = signResponse.data.data["user_agent"];
        }

        signEvents.emit('signSuccess', {
            originalUrl: url,
            signedUrl: signedUrl,
            headers,
            cookieJar,
        });

        return signedUrl;

    } catch (error) {
        signEvents.emit('signError', {
            originalUrl: url,
            headers,
            cookieJar,
            error,
        });

        // If a sessionid is present, the signature is optional => Do not throw an error.
        if (cookieJar.getCookieByName('sessionid')) {
            return url;
        }

        throw new Error(`Failed to sign request: ${error.message}; URL: ${url}`);
    }
}


module.exports = {
    config,
    signEvents,
    signWebcastRequest,
};
