# TikTokPerseusWeb Demo

> Fork from [TikTok-Live-Connector](https://github.com/zerodytrash/TikTok-Live-Connector), we just replace the signature provider with ours.
> Get more information from our [website](https://tt-do2.pages.dev/).


## Installation

```
git clone https://gitlab.com/reverse4free/tiktokperseusweb.git
cd TiktokPerseusWeb
npm install
```

## Usage

- Apply for test key of our signature service, contact https://t.me/reverse4free2 or reverse4free@gmail.com
- Setup the key in `TiktokPerseusWeb/src/lib/tiktokSignatureProvider.js`
```
let config = {
    enabled: true,
    signProviderHost: 'https://new-sign-tt-aycoaohohf.us-west-1.fcapp.run',
    providerPath: "get_web_sign",
    key: "", // contact us[https://t.me/reverse4free2] for applying test key of our signature service D:).
    extraParams: {},
};
```
- Open `TiktokPerseusWeb/src/tiktokPerseusWebDemo.js`
- Input the correct tiktok anchor username who is currently live 
```
// Username of someone who is currently live
let tiktokUsername = "xxxx";
```

## Run

```
npm run test
```

You will get all live information of the specific tiktok anchor:

![live-demo](img/live-demo.png)

**Have fun**!



